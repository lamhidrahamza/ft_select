# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: hlamhidr <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/03/05 21:54:36 by hlamhidr          #+#    #+#              #
#    Updated: 2019/03/31 15:19:44 by hlamhidr         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME2 = libft/libft.a
NAME = ft_select
SRC = *.c
files = *.o
flag = -Wall -Wextra -Werror -ltermcap

all: $(NAME) $(NAME2)

$(NAME):
	@make -C libft
	@gcc -ltermcap -o $(NAME) $(NAME2) $(SRC)

clean:
	/bin/rm -f *.o
	make clean -C libft

fclean: clean
	/bin/rm -f $(NAME)
	make fclean -C libft

re: fclean all
