/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlamhidr <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/24 22:34:10 by hlamhidr          #+#    #+#             */
/*   Updated: 2019/06/24 23:11:06 by hlamhidr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

int		my_outc(int c)
{
	ft_putchar_fd(c,2);
	return (1);
}

int		ft_tablen(char **tb)
{
	int i;

	i = 0;
	while (tb[i])
		i++;
	return (i);
}

t_args	*ft_lstenvnew(char *av)
{
	t_args	*new;

	if (!(new = (t_args *)malloc(sizeof(t_args))))
		return (NULL);
	new->next = NULL;
	new->name = ft_strdup(av);
	new->select = 0;
	return (new);
}

t_args	*add_last(t_args *lst, t_args *new)
{
	t_args *begin;

	begin = lst;
	if (begin == NULL)
		return (new);
	else
	{
		while (begin->next != NULL)
			begin = begin->next;
		begin->next = new;
	}
	return (lst);
}

t_args	*ft_copy_args(char **argv)
{
	t_args	*lst;
	int		j;

	lst = NULL;
	j = 0;
	while (argv[j])
	{
		lst = add_last(lst, ft_lstenvnew(argv[j]));
		j++;
	}
	return (lst);
}

void	ft_set_termcap(void)
{
	struct	termios new;
	char	*term_name;

	if (!(term_name = getenv("TERM")))
	{
		ft_putendl("Term variable not set");
		exit(1);
	}
	if (tgetent(NULL, term_name) == ERR)
	{
		ft_putendl("Term variable not set");
		exit(1);
	}
	tcgetattr(0 ,&new);
	new.c_lflag &= (~ECHO);
	new.c_lflag &= (~ICANON);
	tcsetattr(0 ,TCSANOW,&new);
}

void	ft_mode(char *str, int c)
{
	if (c == 1 || c == 3)
		tputs(tgetstr("us", NULL), 0, my_outc);
	if (c == 2 || c == 3)
		tputs(tgetstr("mr", NULL), 0, my_outc);
	ft_putstr_fd(str, 2);
	if (c == 1 || c == 3)
		tputs(tgetstr("ue", NULL), 0, my_outc);
	if (c == 2 || c == 3)
		tputs(tgetstr("me", NULL), 0, my_outc);
}

void	ft_print_args(t_cursor pos)
{
	int i;
	int index;
	int len;

	i = 0;
	index = 0;
	while (pos.args != NULL)
	{
		if (pos.index == index && pos.args->select)
			ft_mode(pos.args->name, UNDERLINE + RE_VIDEO);
		else if (pos.index == index)
			ft_mode(pos.args->name, UNDERLINE);
		else if (pos.args->select)
			ft_mode(pos.args->name, RE_VIDEO);
		else
			ft_putstr_fd(pos.args->name, 2);
		if (pos.args->next != NULL && pos.num_c != 1)
		{
			len = ft_strlen(pos.args->name);
			len = (pos.long_s + 8) - len;
			while (len--)
				ft_putchar_fd(' ', 2);
			i++;
		}
		if (i >= pos.num_c || pos.num_c == 1)
		{
			i = 0;
			ft_putchar_fd('\n', 2);
		}
		index++;
		pos.args = pos.args->next;
	}
}

void	ft_cursor_down(t_cursor *pos)
{
	if (pos->num_c == 1)
		ft_cursor_right(pos);
	else
	{
		pos->index += pos->num_c;
		if (pos->index > pos->size - 1)
		{	
			pos->index -= pos->size + (pos->num_c - 1) - (pos->size % pos->num_c);
			if (pos->index < 0)
				pos->index += pos->num_c;
		}
		fprintf(pos->fd, "pos->num_c == %d modul == %d  index == %d\n", pos->num_c, pos->size % pos->num_c, pos->index);
		tputs(tgetstr("cl", NULL), 0, my_outc);
		ft_print_args(*pos);
	}
}

void	ft_cursor_up(t_cursor *pos)
{
	if (pos->num_c == 1 || (pos->index == 0 && pos->size % pos->num_c == 0))
		ft_cursor_left(pos);
	else
	{
		pos->index -= pos->num_c;
		if (pos->index < 0)
		{
			pos->index += pos->size + (pos->num_c - 1) - (pos->size % pos->num_c);
			if (pos->index > pos->size - 1)
				pos->index -= pos->num_c;
		}
		tputs(tgetstr("cl", NULL), 0, my_outc);
		ft_print_args(*pos);
	}
	fprintf(pos->fd, "up->index == %d size == %d \n", pos->index, pos->size);

}

void	ft_select_elem(t_cursor *pos)
{
	t_args *begin;
	int index;

	begin = pos->args;
	index = 0;
	tputs(tgetstr("cl", NULL), 0, my_outc);
	while (pos->args != NULL)
	{
		if (pos->index == index)
		{
			if (pos->args->select)
				pos->args->select = 0;
			else
				pos->args->select = 1;
			if (pos->num_c == 1)
				pos->index++;
			else
			{
				pos->index += pos->num_c;
				if (pos->index > pos->size - 1)
				{	
					pos->index -= pos->size + (pos->num_c - 1) - (pos->size % pos->num_c);
					if (pos->index < 0)
						pos->index += pos->num_c;
				}
			}
			break ;
		}
		pos->args = pos->args->next;
		index++;
	}
	pos->args = begin;
	ft_print_args(*pos);
}

void	ft_cursor_right(t_cursor *pos)
{
	tputs(tgetstr("cl", NULL), 0, my_outc);
	if (pos->index == pos->size - 1)
		pos->index = 0;
	else
		pos->index++;
	ft_print_args(*pos);
}

void	ft_select_last_elem(t_cursor *pos)
{
	if (pos->index == 0)
		pos->index = pos->size - 1;
	else
		pos->index--;
}

void	ft_del_elem(t_cursor *pos)
{
	t_args *begin;
	t_args	*tmp;
	int index;

	begin = pos->args;
	tmp = pos->args;
	index = 0;
	tputs(tgetstr("cl", NULL), 0, my_outc);
	while (pos->args != NULL)
	{
		if (pos->index == index)
		{
			if (index == 0)
				begin = pos->args->next;
			else
				tmp->next = pos->args->next;
			free(pos->args->name);
			free(pos->args);
			pos->size--;
			if (!pos->size)
				ft_exit(pos);
			ft_select_last_elem(pos);
			break ;
		}
		tmp = pos->args;
		pos->args = pos->args->next;
		index++;
	}
	pos->args = begin;
	ft_print_args(*pos);
}

void	ft_cursor_left(t_cursor *pos)
{
	tputs(tgetstr("cl", NULL), 0, my_outc);
	if (pos->index == 0)
		pos->index = pos->size - 1;
	else
		pos->index--;
	ft_print_args(*pos);
}

void	ft_exit(t_cursor *pos)
{
	tputs(tgetstr("te", NULL), 0, my_outc);
	tputs(tgetstr("ve", NULL), 0, my_outc);
	exit(0);
}

void	ft_free_lst(t_args *args)
{
	t_args *tmp;

	tmp = NULL;
	while (args != NULL)
	{
		if (args->name)
			free(args->name);
		tmp = args;
		args = args->next;
		if (tmp)
			free(tmp);
	}
}

void	ft_display_and_exit(t_cursor *pos)
{
	t_args	*begin;
	int		i;

	begin = pos->args;
	i = 0;
	tputs(tgetstr("te", NULL), 0, my_outc);
	tputs(tgetstr("ve", NULL), 0, my_outc);
	while (pos->args != NULL)
	{
		if (pos->args->select)
		{
			if (i)
				ft_putchar(' ');
			else
				i = 1;
			ft_putstr(pos->args->name);
			
		}
		pos->args = pos->args->next;
	}
	pos->args = begin;
	ft_free_lst(pos->args);
	exit(0);
}

void	ft_read_keys(t_cursor *pos)
{
	char buf[6];
	int len;

	pos->fd = fopen("/dev/ttys000", "a+");
	ft_bzero(buf, 6);
	while ((len = read(0, buf, 6)))
	{
		fprintf(pos->fd, "buf[0]== %d buf[1] == %d buf[2] == %d, buf[3] == %d buf[4] == %d buf[5] == %d\n", buf[0], buf[1], buf[2], buf[3], buf[4], buf[5]);
		if (ESC(buf))
			ft_exit(pos);
		if (ENTER(buf))
			ft_display_and_exit(pos);
		else if (SELECT(buf))
			ft_select_elem(pos);
		else if (DO(buf))
			ft_cursor_down(pos);
		else if (UP(buf))
			ft_cursor_up(pos);
		else if (RI(buf))
			ft_cursor_right(pos);
		else if (LE(buf))
			ft_cursor_left(pos);
		else if (DEL(buf))
			ft_del_elem(pos);
		ft_bzero(buf, 6);
	}
}
void	ft_get_size_windz(t_cursor *pos)
{
	struct winsize ws;
	ioctl(0, TIOCGWINSZ, &ws);
	pos->col = ws.ws_col;
	pos->row = ws.ws_row;
}

int		ft_long_str(char **av)
{
	int j;
	int i;
	int save;

	j = 0;
	i = 0;
	save = 0;
	while (av[j])
	{
		if ((i = ft_strlen(av[j])) > save)
			save = i;
		j++;
	}
	return (save);
}

void	ft_check_and_print(t_cursor pos)
{
	if (!pos.num_c || (pos.size / pos.num_c) > pos.row - 1 || pos.long_s + 8 > pos.col)
			ft_putstr("The size of the window very small !");
		else
			ft_print_args(pos);
}

void	ft_win_change(int si)
{
	si = 0;
	tputs(tgetstr("cl", NULL), 0, my_outc);
	ft_get_size_windz(&for_sig);
	for_sig.num_c = (for_sig.col - 1) / (for_sig.long_s + 8);
	ft_check_and_print(for_sig);
}

int main(int ac, char **av)
{
	t_cursor pos;

	ft_set_termcap();
	tputs(tgetstr("ti", NULL), 0, my_outc);
	tputs(tgetstr("vi", NULL), 0, my_outc);
	ft_get_size_windz(&pos);
	pos.long_s = ft_long_str(av);
	pos.args = ft_copy_args(av + 1);
	pos.size = ft_tablen(av) - 1;
	pos.index = 0;
	pos.num_c = (pos.col - 1) / (pos.long_s + 8);
	ft_check_and_print(pos);
	signal(SIGWINCH, ft_win_change);
	while (1337)
	{
		ft_memcpy(&for_sig, &pos, sizeof(t_cursor));
		ft_read_keys(&pos);
		
	}
	return (0);
}
