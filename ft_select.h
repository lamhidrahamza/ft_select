/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hlamhidr <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/24 22:30:48 by hlamhidr          #+#    #+#             */
/*   Updated: 2019/06/24 23:04:39 by hlamhidr         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SELECT_H
#define FT_SELECT_H

#include "libft/libft.h"
#include <termios.h>
#include <sys/ioctl.h>
#include <term.h>
#include <curses.h>
#define DO(x) (x[0] == 27 && buf[1] == 91 && buf[2] == 66 && buf[3] == 0 && buf[4] == 0 && buf[5] == 0)
#define UP(x) (x[0] == 27 && buf[1] == 91 && buf[2] == 65 && buf[3] == 0 && buf[4] == 0 && buf[5] == 0)
#define RI(x) (x[0] == 27 && buf[1] == 91 && buf[2] == 67 && buf[3] == 0 && buf[4] == 0 && buf[5] == 0)
#define LE(x) (x[0] == 27 && buf[1] == 91 && buf[2] == 68 && buf[3] == 0 && buf[4] == 0 && buf[5] == 0)
#define ENTER(x) (x[0] == 10 && buf[1] == 0 && buf[2] == 0 && buf[3] == 0 && buf[4] == 0 && buf[5] == 0)
#define SELECT(x) (x[0] == 32 && buf[1] == 0 && buf[2] == 0 && buf[3] == 0 && buf[4] == 0 && buf[5] == 0)
#define DEL(x) (x[0] == 127 && buf[1] == 0 && buf[2] == 0 && buf[3] == 0 && buf[4] == 0 && buf[5] == 0)
#define ESC(x) (x[0] == 27 && buf[1] == 0 && buf[2] == 0 && buf[3] == 0 && buf[4] == 0 && buf[5] == 0)
#define UNDERLINE 1
#define RE_VIDEO 2

typedef struct		s_args
{
	char			*name;
    int				select;
	struct s_args	*next;
}					t_args;

typedef struct		s_cursor
{
    int				col;
    int				row;
	int				size;
    int				long_s;
	int				index;
    int             num_c;
    FILE *fd;
    t_args			*args;
}					t_cursor;

t_cursor            for_sig;

void	ft_cursor_right(t_cursor *pos);
void	ft_cursor_left(t_cursor *pos);
void	ft_exit(t_cursor *pos);

#endif
